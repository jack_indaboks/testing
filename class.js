export default class TestClass {
	constructor(name="World",greet="Hello") {
		this.name = name
		this.greet = greet
	}
	sayHello() {
		let greeting = this.greet + " " + this.name
		console.log(greeting)
		return greeting
	}
}
